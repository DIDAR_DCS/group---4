<?php
include_once($_SERVER["DOCUMENT_ROOT"]."/phpcrud/bootstrap.php");
//selection query
$id = $_GET['id'];
$query = 'SELECT * FROM subscribers WHERE id = :id';
$sth = $conn->prepare($query);
$sth->bindParam(':id',$id);
$sth->execute();
$subscribers = $sth->fetch(PDO::FETCH_ASSOC);

?>

<?php
ob_start();
?>

    <main role="main" class="col-md-9 ml-sm-auto col-lg-10 px-4">
        <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3">
            <h1 >Subscribers</h1>
            <div class="btn-toolbar mb-2 mb-md-0">
                <button type="button" class="btn btn-sm btn-outline-secondary">
                    <span data-feather="calendar"></span>
                    <a href="<?=VIEW;?>subscribers/index.php" style="color: black">Go to list</a>
                </button>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12 ">

                <div class="row">
                    <div class="col-sm col-md-6 col-lg-3 ">
                            <div class="text ">
                                <h3><a href="#"><?php echo $subscribers['ID']?></a></h3>
                                    <?php echo $subscribers['email']?>
                                    <?php echo $subscribers['is_subscribed']?>
                                    <?php echo $subscribers['created_at']?>
                                    <?php echo $subscribers['modified_at']?>
                                    <?php echo $subscribers['reason']?>

                        </div>
                    </div>

                </div>

            </div>
        </div>



    </main>

<?php
$pagecontent = ob_get_contents();
ob_end_clean();
echo str_replace("##MAIN_CONTENT##",$pagecontent,$layout);
?>