
<?php
include_once($_SERVER["DOCUMENT_ROOT"] . "/phpcrud/bootstrap.php");
ob_start();
?>

<main role="main" class="col-md-9 ml-sm-auto col-lg-10 px-4">
    <form id="subscribers-form"
          method="post"
          action="store.php"
          enctype="multipart/form-data"
          role="form">
        <div class="messages"></div>
        <h1>ADD NEW</h1>
<section>
    <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">

                    <div class="row">
                        <div class="col-md-5">
<!--                            <div class="form-group">-->
<!--                                <label for="id">ID</label>-->
<!--                                <input type="number"-->
<!--                                       class="form-control"-->
<!--                                       id="id"-->
<!--                                       name="id"-->
<!--                                       value=""-->
<!--                                       aria-describedby="id"-->
<!--                                       placeholder="">-->
<!--                            </div>-->

                            <div class="form-group">
                                <label for="email">E-Mail</label>
                                <input type="text"
                                       class="form-control"
                                       id="email"
                                       name="email"
                                       value=""
                                       aria-describedby="email"
                                       placeholder=""
                                       autofocus="autofocus">
                            </div>

<!--                            <div class="form-group">-->
<!--                                <label for="is_subscribed">Is Subscribed</label>-->
<!--                                <input type="text"-->
<!--                                       class="form-control"-->
<!--                                       id="is_subscribed"-->
<!--                                       name="is_subscribed"-->
<!--                                       value=""-->
<!--                                       aria-describedby="is_subscribed"-->
<!--                                       placeholder="">-->
<!--                            </div>-->

                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label for="is_subscribed">Active</label>
                                    <input id="is_subscribed" checked="checked" value="1" type="checkbox" name="is_subscribed" class="form-control">
                                    <div class="help-block with-errors"></div>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="created_at">Created AT</label>
                                <input type="datetime"
                                       class="form-control"
                                       id="created_at"
                                       name="created_at"
                                       value="<?php echo date("Y/m/d")?> "
                                       aria-describedby="created_at"
                                       placeholder="">
                            </div>


                            <div class="form-group">
                                <label for="modified_at">Modified At</label>
                                <input type="datetime"
                                       class="form-control"
                                       id="modified_at"
                                       name="modified_at"
                                       value="<?php echo date("Y/m/d")?>"
                                       aria-describedby="modified_at"
                                       placeholder="">
                            </div>

                            <div class="form-group">
                                <label for="reason">Reason</label>
                                <input type="text"
                                       class="form-control"
                                       id="reason"
                                       name="reason"
                                       value=""
                                       aria-describedby="modified_at"
                                       placeholder="">
                            </div>



                        </div>

                    </div>
                </div>
            </div>

            <button type="submit" class="btn btn-success">
                Send & Save Product
            </button>
        </form>
    </div>
</section>
</main>
    <?php
    $pagecontent = ob_get_contents();
    ob_end_clean();
    echo str_replace("##MAIN_CONTENT##", $pagecontent, $layout);
    ?>

