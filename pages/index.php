<?php
include_once($_SERVER["DOCUMENT_ROOT"] . "/phpcrud/bootstrap.php");
//selection query
$query = "SELECT * FROM pages";
$sth = $conn->prepare($query);
$sth->execute();
$pages = $sth->fetchAll(PDO::FETCH_ASSOC);
//foreach($products as $row){
//echo "<pre>";
//print_r($row['mrp']);
//echo "</pre>";
//
//}
//die();
// echo "<li>".$product['title']."</li>";

?>
<?php
ob_start();
?>

    <main role="main" class="col-md-9 ml-sm-auto col-lg-10 px-4">
        <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3">
            <h1>Pages</h1>
            <div class="btn-toolbar mb-2 mb-md-0">
                <button type="button" class="btn btn-sm btn-outline-secondary">
                    <span data-feather="calendar"></span>
                    <a href="<?=VIEW;?>pages/create.php"style="color: black">Add New</a>
                </button>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12 ftco-animate">
                <div class="cart-list">
                    <table class="table">
                        <thead class="thead-primary">
                        <tr class="text-center">
                            <th>&nbsp;</th>
                            <th>Title</th>
                            <th>Description</th>
                            <th>Link</th>
                            <th>Action</th>


                        </tr>
                        </thead>
                        <tbody>
                        <?php
                        if ($pages) {
                            foreach ($pages as $page) {
                                ?>
                                <tr class="text-center">
                                    <td class="page-item">&nbsp;
                                    </td>

                                    <td class="page-title">
                                        <h3><a href="#?id=<?php echo $page['id'] ?>"><?php echo $page['title']; ?></a>
                                        </h3>
                                    <td class="page-description">

                                    <p>
                                            <?php echo $page['description']; ?>
                                        </p>
                                        </td>
                                    <td class="page-title">

                                        <p>
                                            <a href="#?id=<?php echo $page['id'] ?>"><?php echo $page['link']; ?></a>
                                        </p>
                                    </td>
                                    </td>
                                    <td><a href="edit.php?id=<?php echo $page['id'] ?>">Edit</a>
                                        | <a href="delete.php?id=<?php echo $page['id'] ?>">Delete</a>
                                    </td>


                                </tr>
                            <?php }
                        } else {
                            ?>

                            <tr class="text-center">
                                <td colspan="5">
                                    There is no page available. <a href="create.php">Click Here</a> to add a product.
                                </td>
                            </tr>
                            <?php
                        } ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>


    </main>


<?php
$pagecontent = ob_get_contents();
ob_end_clean();
echo str_replace("##MAIN_CONTENT##", $pagecontent, $layout);
?>