<?php
include_once($_SERVER["DOCUMENT_ROOT"]."/phpcrud/bootstrap.php");


$target_file = $_FILES['picture']['tmp_name'];
$filename = time()."_".str_replace(' ','-',$_FILES['picture']['name']);
$dest_file = $_SERVER['DOCUMENT_ROOT'].'/phpcrud/uploads/'.$filename;
$is_uploaded = move_uploaded_file($target_file, $dest_file);

if($is_uploaded){
    $dest_filename = $filename;
}else{
    $dest_filename = "";
}

//collect the data
$title = $_POST['title'];
$description = $_POST['description'];
$link = $_POST['link'];

$is_active = 0;
if(array_key_exists('is_active',$_POST)){
    $is_active = $_POST['is_active'];
}

//prepare the insert query
//selection query
$query = "INSERT INTO `pages` 
(`id`, 
`title`, 
`description`, 
`link`)
 VALUES 
 (NULL, 
 :title, 
 :description, 
 :link);";


$sth = $conn->prepare($query);
$sth->bindParam(':title',$title);
$sth->bindParam(':description',$description);
$sth->bindParam(':link',$link);

$result = $sth->execute();


//redirect to index page
header("location:index.php");
